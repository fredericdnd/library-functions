import * as admin from "firebase-admin";
import * as functions from "firebase-functions";
import BookService from "./data/services/book-service";
import StockService from "./data/services/stock-service";

admin.initializeApp();

const bookService = BookService.shared;
const stockService = StockService.shared;

export const createBook = functions.https.onRequest((req: any, res: any) => {
  const name = req.body.name;
  const author = req.body.author;
  const datePublished = req.body.datePublished;
  const price = req.body.price;

  if (req.method !== "POST") {
    return res.status(403).send("Unauthorized");
  } else if (name === undefined) {
    return res.status(400).send("Invalid name");
  } else if (author === undefined) {
    return res.status(400).send("Invalid author");
  } else if (datePublished === undefined) {
    return res.status(400).send("Invalid datePublished");
  }

  return bookService
    .createBook(name, author, datePublished, price)
    .then(() => {
      res.status(201).send("Book created");
    })
    .catch((error) => {
      res.status(400).send(error);
    });
});

export const updateBook = functions.https.onRequest((req: any, res: any) => {
  const id = req.body.id;
  const name = req.body.name;
  const author = req.body.author;
  const datePublished = req.body.datePublished;
  const price = req.body.price;

  if (req.method !== "PATCH") {
    return res.status(403).send("Unauthorized");
  }

  bookService
    .updateBook(id, name, author, datePublished, price)
    .then(() => {
      res.status(201).send("Book updated");
    })
    .catch((error) => {
      res.status(400).send(error);
    });
});

export const deleteBook = functions.https.onRequest((req: any, res: any) => {
  const id = req.body.id;

  if (req.method !== "DELETE") {
    return res.status(403).send("Unauthorized");
  } else if (id === undefined) {
    return res.status(400).send("Invalid id");
  }

  return bookService
    .deleteBook(id)
    .then(() => {
      res.status(201).send("Book deleted");
    })
    .catch((error) => {
      res.status(400).send(error);
    });
});

export const getBook = functions.https.onRequest((req: any, res: any) => {
  const id = req.query.id;

  if (req.method !== "GET") {
    return res.status(403).send("Unauthorized");
  } else if (id === undefined) {
    return res.status(400).send("Invalid id");
  }

  return bookService
    .getBook(id)
    .then((book) => {
      res.status(201).send(book);
    })
    .catch((error) => {
      console.log(error);
      res.status(400).send(error);
    });
});

export const getBooks = functions.https.onRequest((req: any, res: any) => {
  const ids = req.query.ids;

  if (req.method !== "GET") {
    return res.status(403).send("Unauthorized");
  } else if (ids === undefined || ids.length === 0) {
    return res.status(400).send("Invalid ids");
  }

  return bookService
    .getBooks(ids)
    .then((books) => {
      res.status(201).send(books);
    })
    .catch((error) => {
      res.status(400).send(error);
    });
});

export const updateBookStock = functions.https.onRequest(
  (req: any, res: any) => {
    const bookId = req.body.bookId;
    const stock = Number(req.body.stock);

    if (req.method !== "POST") {
      return res.status(403).send("Unauthorized");
    } else if (bookId === undefined) {
      return res.status(400).send("Invalid bookId");
    } else if (stock === undefined) {
      return res.status(400).send("Invalid stock");
    }

    stockService
      .updateBookStock(bookId, stock)
      .then(() => {
        res.status(201).send("Book stock updated");
      })
      .catch((error) => {
        res.status(400).send(error);
      });
      
  }
);
