import { firestore } from "firebase-admin";

export default class StockService {
  private static instance: StockService;

  collection = firestore().collection("stocks");

  static get shared(): StockService {
    if (!this.instance) {
      this.instance = new this();
    }

    return this.instance;
  }

  public updateBookStock(id: string, stock: number) {
    const book: any = {};

    book[id] = firestore.FieldValue.increment(stock);

    return this.collection.doc("books").update(book);
  }
}
