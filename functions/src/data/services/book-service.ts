import { firestore } from "firebase-admin";
import Book from "../models/book";

export default class BookService {
  private static instance: BookService;

  collection = firestore().collection("books");

  static get shared(): BookService {
    if (!this.instance) {
      this.instance = new this();
    }

    return this.instance;
  }

  public createBook(
    name: string,
    author: string,
    datePublished: string,
    price: number,
  ) {
    const id = this.collection.doc().id;

    const book: any = {
      id: id,
      name: name,
      author: author,
      datePublished: datePublished,
      price: price,
    };

    return this.collection.doc(id).set(book);
  }

  public updateBook(
    id: string,
    name: string | null,
    author: string | null,
    datePublished: string | null,
    price: number | null
  ) {
    const book: any = {
      id: id,
      name: name,
      author: author,
      datePublished: datePublished,
      price: price,
    };

    return this.collection.doc(id).update(JSON.parse(JSON.stringify(book)));
  }

  public deleteBook(id: string) {
    return this.collection.doc(id).delete();
  }

  public getBook(id: string) {
    return this.collection
      .doc(id)
      .get()
      .then((snapshot) => Book.map(snapshot.data()));
  }

  public getBooks(ids: [string]) {
    const promises: Promise<Book>[] = [];

    ids.forEach((id) => {
      promises.push(this.getBook(id));
    });

    return Promise.all(promises);
  }
}
