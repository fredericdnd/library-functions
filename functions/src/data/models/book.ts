export default class Book {
  id: string;
  name: string;
  author: string;
  datePublished: string;
  price: number;

  constructor(
    id: string,
    name: string,
    author: string,
    datePublished: string,
    price: number,
  ) {
    this.id = id;
    this.name = name;
    this.author = author;
    this.datePublished = datePublished;
    this.price = price;
  }

  public static map(json: any): Book {
    return new this(
      json.id,
      json.name,
      json.author,
      json.datePublished,
      json.price,
    );
  }
}
